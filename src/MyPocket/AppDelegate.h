//
//  AppDelegate.h
//  MyPocket
//
//  Created by 鈴木季子 on 2014/06/18.
//  Copyright (c) 2014年 Robust-inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
