//
//  TopBookView.m
//  MyPocket
//
//  Created by 松澤隆之 on 2014/06/19.
//  Copyright (c) 2014年 Robust-inc. All rights reserved.
//

#import "TopBookView.h"

@implementation TopBookView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		[self _makeView];
    }
    return self;
}


- (id)init
{
    self = [super init];
    if (self) {
        [self _makeView];
    }
    return self;
}

-(void)_makeView
{
    [[NSBundle mainBundle] loadNibNamed:@"TopBookView" owner:self options:nil];
    [self addSubview:self.contentView];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
