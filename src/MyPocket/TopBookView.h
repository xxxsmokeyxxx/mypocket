//
//  TopBookView.h
//  MyPocket
//
//  Created by 松澤隆之 on 2014/06/19.
//  Copyright (c) 2014年 Robust-inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopBookView : UIView
@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet UIImageView *albumImageView;

@end
