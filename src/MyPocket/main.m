//
//  main.m
//  MyPocket
//
//  Created by 鈴木季子 on 2014/06/18.
//  Copyright (c) 2014年 Robust-inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
