//
//  TopViewController.m
//  MyPocket
//
//  Created by 鈴木季子 on 2014/06/18.
//  Copyright (c) 2014年 Robust-inc. All rights reserved.
//

#import "TopViewController.h"
#import "TopBookView.h"
#import "iCarousel.h"

@interface TopViewController () <iCarouselDataSource, iCarouselDelegate>
{
	NSInteger pageNo;
}

@property (weak, nonatomic) IBOutlet iCarousel *oCarousel;
@property (nonatomic) NSMutableArray *bookImageViews;
@property (strong, nonatomic) IBOutlet UIImageView *backImageView;

@end

@implementation TopViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	self.oCarousel.type = iCarouselTypeLinear;
	self.oCarousel.bounceDistance = 0.2;
	self.oCarousel.currentItemIndex = 0;

	
	CGFloat x = 42;
	TopBookView *bookView = [[TopBookView alloc] initWithFrame:CGRectMake(x, 0, 220, 310)];
	TopBookView *bookView2 = [[TopBookView alloc] initWithFrame:CGRectMake(x, 0, 220, 310)];
	[bookView2.albumImageView setImage:[UIImage imageNamed:@"album_scene02_l"]];
	TopBookView *bookView3 = [[TopBookView alloc] initWithFrame:CGRectMake(x, 0, 220, 310)];
	[bookView3.albumImageView setImage:[UIImage imageNamed:@"album_scene03_l"]];

	self.bookImageViews = [NSMutableArray array];
	[self.bookImageViews addObject:bookView];
	[self.bookImageViews addObject:bookView2];
	[self.bookImageViews addObject:bookView3];

	[self.oCarousel reloadData];

//	self.bookScrollView.contentSize = CGSizeMake(304*3, self.bookScrollView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)topViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
//    NSLog(@"First view return action invoked.");
}

- (IBAction)backAction:(id)sender {
}
- (IBAction)nextAction:(id)sender {
}


#pragma mark - iCarousel Datasource
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
	return self.bookImageViews.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view {
	return self.bookImageViews[index];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
	if (option == iCarouselOptionSpacing) {
		// セルとセルの間のスペース
		return 1.1;
	}
	return value;
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
	NSString * imageName = @"";
	if (self.oCarousel.currentItemIndex == 0) {
		imageName = @"album_scene01_view";
	} else if (self.oCarousel.currentItemIndex == 1) {
		imageName = @"album_scene02_l";
	} else if (self.oCarousel.currentItemIndex == 2) {
		imageName = @"album_scene03_l";
	}
	[self.backImageView setImage:[UIImage imageNamed:imageName]];
}


@end
